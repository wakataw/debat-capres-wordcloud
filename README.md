# Debat Capres 2 Word Cloud

## Cara Menjalankan

1. Clone repository ini
    ```bash
    git clone https://gitlab.com/wakataw/debat-capres-wordcloud.git
    cd debat-capres-wordcloud
    ```
2. Pastikan `jupyter notebook` sudah terpasang.
3. Install *package* yang diperlukan
    ```bash
    pip install -r requirements.txt
    ```
4. Buka notebook dengan menjalankan perintah `jupyter notebook --notebook-dir=.` atau upload notebook ke jupyter notebook server beserta dengan berkas pendukungnya.

## Hasil

Jokowi | Prabowo
---|---
![Jokowi](results/1.png)|![Prabowo](results/2.png)

## License

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work and the instructional material is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.